Vimeo Video Gallery
=================

The module provides list of vimeo videos from its album, group, channel or user.

To use vimoevideogallery operations :-

1) Install the vimoevideogallery module
2) Enable the vimoevideogallery block
